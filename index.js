/**
 * @format
 */

import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {Provider} from './src/store/configureStore';

GLOBAL.XMLHttpRequest = GLOBAL.originalXMLHttpRequest || GLOBAL.XMLHttpRequest;

const RNReducer = () => (
  <Provider>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => RNReducer);
