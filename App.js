/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  LogBox,
  TouchableOpacity
} from 'react-native';
import TodoListForm from './src/components/TodoListForm/TodoListForm';
import TodoTable from './src/components/TodoTable/TodoTable';
import Modal from 'react-native-modal';
import {mainAppContext} from './src/store/configureStore';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  onAddTodo,
  onDeleteTodo,
  onUpdateTodo,
  onPersistTodo
} from './src/store/actions/action';
import AsyncStorage from '@react-native-community/async-storage';

LogBox.ignoreLogs([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
])

const App = () => {

  const {state, dispatch} = mainAppContext();
  const {todoList} = state;

  const [showModal, setShowModal] = useState(false);

  const [editId, setEditId] = useState(null);
  const [editTitle, setEditTitle] = useState(null);
  const [editType, setEditType] = useState(null);

  useEffect(() => {
    persistData();
  }, [])

  const persistData = async () => {
    try {
      const data = await AsyncStorage.getItem("todoList");
      if(data){
        dispatch(onPersistTodo(JSON.parse(data)));
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    AsyncStorage.setItem("todoList", JSON.stringify(todoList));
  })

  const closeModal = () => {
    setShowModal(false);
  }

  const openModal = (id,title, type) => {
    setShowModal(true);
    setEditId(id)
    setEditTitle(title);
    setEditType(type);
  }

  const addTodo = (title, type) => {
    if(!title  || !type) {
      alert('Please fill all required fields');
      return false;
    }
    dispatch(onAddTodo({id: Date.now(), title: title, type: type}));
  }

  const deleteTodo = (id) => {
    dispatch(onDeleteTodo(id))
  }

  const updateTodo = (title, type) => {
    dispatch(onUpdateTodo({id: editId, title: title, type: type}));
    setShowModal(false);
  }
 

  return (
    <>
      <SafeAreaView
        style={styles.container}
      >
        <ScrollView
          contentContainerStyle={styles.scrollView}
          >
            <View>
              <Text style={styles.appTitle}>
                Todo Application
              </Text>
            </View>

            <TodoListForm 
              editTitle={editTitle}
              editType={editType}
              buttonTitle="Add Todo"
              onPressButton={addTodo}
              isEdit={false}
            />

            <TodoTable
              deleteTodo={deleteTodo}
              openModal={openModal}
            />

            <View>
              <Modal isVisible={showModal} onBackdropPress={closeModal}>
                <View style={styles.modalContainer}>
                <TouchableOpacity onPress={closeModal}>
                  <View style={styles.todoTableActionEdit}>
                      <Icon name="close" size={18} color="#333333" />
                  </View>
                </TouchableOpacity>
                  <TodoListForm 
                    editTitle={editTitle}
                    editType={editType}
                    onPressButton={updateTodo}
                    isEdit={true}
                    buttonTitle="Update Todo"
                  />
                </View>
              </Modal>
            </View>
            
        </ScrollView>
      </SafeAreaView>
    </>
  );
};



const styles = StyleSheet.create({
  container:{
    justifyContent: 'center',
  },
  scrollView:{
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 30,
    paddingBottom: 30
  },
  appTitle:{
    fontSize: 28,
    textAlign: 'center',
    marginBottom: 15
  },
  modalContainer:{
    backgroundColor: '#ffffff',
    padding: 20,
    position: 'relative'
  },
  todoTableActionEdit:{
    alignItems: 'flex-end',
    marginBottom: 15,
  }
});

export default App;
