import React, {createContext, useReducer, useContext} from 'react';
import {initialState, reducer} from './reducer/reducer';

export const AppContext = createContext();

export const mainAppContext = () => {
  const contextValue = useContext(AppContext);
  return contextValue;
};

export const Provider = ({children}) => {
  let [state, dispatch] = useReducer(reducer, initialState);
  let value = {state, dispatch};

  return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};
