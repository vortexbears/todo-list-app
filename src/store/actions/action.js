import {
    ON_ADD_TODO,
    ON_UPDATE_TODO,
    ON_DELETE_TODO,
    ON_PERSIST_TODO
} from './actionTypes';

export const onAddTodo = data => {
  return {
    type: ON_ADD_TODO,
    payload: data,
  };
};

  
  export const onUpdateTodo = data => {
    return {
      type: ON_UPDATE_TODO,
      payload: data
    };
  };

  export const onDeleteTodo = data => {
    return {
      type: ON_DELETE_TODO,
      payload: data
    };
  };
  
  export const onPersistTodo = data => {
    return {
      type: ON_PERSIST_TODO,
      payload: data,
    };
  };
  
 