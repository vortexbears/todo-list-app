export const ON_ADD_TODO = 'ON_ADD_TODO';
export const ON_UPDATE_TODO = 'ON_UPDATE_TODO';
export const ON_DELETE_TODO = 'ON_DELETE_TODO';
export const ON_PERSIST_TODO = 'ON_PERSIST_TODO';