import {
    ON_ADD_TODO,
    ON_DELETE_TODO,
    ON_UPDATE_TODO,
    ON_PERSIST_TODO
} from '../actions/actionTypes';
  
export const initialState = {
    todoList: [
      {
        id: 123,
        title: 'Todo List 1',
        type: 'Done'
      },
      {
        id: 124,
        title: 'Todo List 2',
        type: 'In Progress'
      }
    ]
};
  
  export const reducer = (state = initialState, action) => {
    switch (action.type) {
      case ON_ADD_TODO: {
        return {
          ...state,
          todoList: state.todoList.concat(action.payload)
        };
      }
      case ON_UPDATE_TODO: {
          let idAlreadyExists = state.todoList.some(el => el.id == action.payload.id)
          // make a copy of the existing array
          let todoList = state.todoList.slice();
          if(idAlreadyExists) {
              todoList.forEach((el, index) => {
                    if(el.id === action.payload.id){
                      Object.assign(todoList[index], action.payload);
                    }
              });
          } 
        return {
          ...state,
          todoList
        };
      }
      case ON_DELETE_TODO: {
        return {
          ...state,
          todoList: state.todoList.filter(val => {
            return val.id !== action.payload
          })
        };
      }
      case ON_PERSIST_TODO: {
        return {
          ...state,
          todoList: action.payload
        };
      }
      default:
        return state;
    }
  };
  