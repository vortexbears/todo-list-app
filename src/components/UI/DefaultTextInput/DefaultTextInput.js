import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';

const DefaultTextInput = props => {
    return (
        <View>
            <TextInput
                {...props}
                style={styles.textInput}
            /> 
        </View>
    )
}

const styles = StyleSheet.create({
    textInput: {
        borderWidth: 1,
        padding: 10,
        borderRadius: 6,
        borderColor: '#333333',
        height: 40,
        fontSize: 14,
        backgroundColor: '#ffffff'
    }
})

export default DefaultTextInput;