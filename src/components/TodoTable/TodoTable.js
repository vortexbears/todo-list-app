import React, { useState } from 'react';
import {View, StyleSheet, Text, FlatList, TouchableOpacity} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {mainAppContext} from '../../store/configureStore';

const dropdownItems = [
    {label: 'All', value: 'All'},
    {label: 'Done', value: 'Done'},
    {label: 'In Progress', value: 'In Progress'}
]

const TodoTable = ({deleteTodo, openModal}) => {

    const {state, dispatch} = mainAppContext();
    const {todoList} = state;
    const [filterArray, setFilterArray] = useState('All');

    const onFilterItem = (val) => {
        setFilterArray(val);
    }

    const RenderFilterItem = ({id, title, type}) => {
        return (
            <View style={styles.todoTableData}>
            <View style={styles.todoTableTextContent}>
                <Text>
                    {title}
                </Text>
            </View>
            <View style={styles.todoTableTextContent}>
                <Text>
                    {type}
                </Text>
            </View>
            <View style={styles.todoTableActions}>
                <TouchableOpacity onPress={() => openModal(id, title, type)}>
                    <View style={styles.todoTableActionEdit}>
                        <Icon name="edit" size={18} color="#54B75E" />
                    </View>
                </TouchableOpacity>

                <View style={styles.todoTableActionDelete}>
                    <TouchableOpacity onPress={() => deleteTodo(id)}>
                        <Icon name="delete" size={18} color="#C0392B" />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
        )
    }
    const _renderItem = ({item}) =>  {

        const {id, title, type} = item;

        if(filterArray === "All"){
            return <RenderFilterItem id={id} title={title} type={type} />
        }
        
        if(filterArray == type) {
            return <RenderFilterItem id={id} title={title} type={type} />
        }
    }

    const _keyExtractor = (item, index) => index.toString()

    return (
        <>
            <View style={styles.todoTableContainer}>
                <View>
                </View>
                <View style={styles.todoTableDropdown}>
                    <DropDownPicker
                    items={dropdownItems}
                    containerStyle={{height: 30}}
                    style={styles.dropdownStyle}
                    itemStyle={{
                        justifyContent: 'flex-start'
                    }}
                    dropDownStyle={{backgroundColor: '#ffffff'}}
                    onChangeItem={item => onFilterItem(item.value)}
                    placeholder="All"
                    styles={styles.dropdownStyle}
                />
                </View>
            </View>
            <View>
                <FlatList 
                    data={todoList}
                    // columnWrapperStyle={{flex: 1, justifyContent: 'space-around'}}
                    // numColumns={2}
                    renderItem={_renderItem}
                    keyExtractor={_keyExtractor}
                />
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    todoTableContainer:{
        marginTop: 30,
        flexDirection: 'row'
    },
    todoTableDropdown:{
        flexBasis: '40%'
    },
    margin4:{
        marginBottom: 15
    },
    dropdownStyle:{
        borderColor: '#333333',
        borderRadius: 6,
    },
    todoTableData:{
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 15,
        borderBottomWidth: 1
    },
    todoTableTextContent:{
        flexBasis: '40%',
        padding: 10
    },
    todoTableActions:{
        padding: 10,
        flexBasis: '20%',
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    todoTableActionDelete:{
        marginLeft: 10
    }
})

export default TodoTable;