import React, { useState } from 'react';
import {View, StyleSheet, Button} from 'react-native';
import DefaultTextInput from '../UI/DefaultTextInput/DefaultTextInput';
import DropDownPicker from 'react-native-dropdown-picker';



const TodoListForm = ({buttonTitle, onPressButton, editTitle, editType, isEdit}) => {

    const dropdownItems = [
        {label: 'Done', value: 'Done', selected: (editType == 'Done') ? true : false},
        {label: 'In Progress', value: 'In Progress', selected: (editType == 'In Progress') ? true : false}
    ]

    const [title, setTitle] = useState(editTitle);
    const [dropdownType, setDropdownType] = useState(editType);

    const onAddForm = () => {
        setTitle(null);
        setDropdownType(null);
        onPressButton(title, dropdownType);
    }

    return (
        <View>
            <View style={styles.margin4}>
                <DefaultTextInput 
                    placeholder="Title *"
                    placeholderTextColor="#333333"
                    onChangeText={setTitle}
                    value={title}
                />
            </View>
            <View style={styles.margin4}>
                <DropDownPicker
                items={dropdownItems}
                containerStyle={{height: 40}}
                style={styles.dropdownStyle}
                itemStyle={{
                    justifyContent: 'flex-start'
                }}
                dropDownStyle={{backgroundColor: '#ffffff'}}
                onChangeItem={item => setDropdownType(item.value)}
                styles={styles.dropdownStyle}
                defaultValue={dropdownType}
                placeholder="Status *"
            />
            </View>
            <View style={styles.buttonContainer}>
                <Button 
                    onPress={() => onAddForm()} 
                    title={buttonTitle}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    margin4:{
        marginBottom: 15
    },
    dropdownStyle:{
        borderColor: '#333333',
        borderRadius: 6
    },
    buttonContainer:{
        zIndex: -1
    }
})

export default TodoListForm;